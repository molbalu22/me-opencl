#include <stdio.h>
#include <stdlib.h>

#include <CL/cl.h>

#include "mycutil.h"

#ifndef KERNEL_FILE
#define KERNEL_FILE "kernel01.cl"
#endif
char *MACRO_KERNEL_FILE = KERNEL_FILE;

const int SAMPLE_SIZE = 1000;

int main(void) {
    int i;
    cl_int err;

    // Get platform
    cl_uint n_platforms;
    cl_platform_id platform_id;
    err = clGetPlatformIDs(1, &platform_id, &n_platforms);
    if (err != CL_SUCCESS) {
        printf("[ERROR] Error calling clGetPlatformIDs. Error code: %d\n", err);
        return 0;
    }

    // Get device
    cl_device_id device_id;
    cl_uint n_devices;
    err = clGetDeviceIDs(
        platform_id, CL_DEVICE_TYPE_GPU, 1, &device_id, &n_devices
    );
    if (err != CL_SUCCESS) {
        printf("[ERROR] Error calling clGetDeviceIDs. Error code: %d\n", err);
        return 0;
    }

    // Create OpenCL context
    cl_context context = clCreateContext(0, n_devices, &device_id, 0, 0, 0);

    // Load kernel code
    char const *kernel_code = read_all_text(KERNEL_FILE);

    if (kernel_code == 0) {
        perror("kernel code not loaded");
        return EXIT_FAILURE;
    }

    // Build the program
    cl_program program =
        clCreateProgramWithSource(context, 1, &kernel_code, 0, 0);
    free((void *)kernel_code);

    err = clBuildProgram(program, 0, 0, 0, 0, 0);
    if (err != CL_SUCCESS) {
        printf("Build error! Code: %d\n", err);
        return 0;
    }

    cl_kernel kernel = clCreateKernel(program, "hello_kernel", 0);

    // Create the host buffer and initialize it
    int *host_buffer = (int *)malloc(SAMPLE_SIZE * sizeof(int));
    for (i = 0; i < SAMPLE_SIZE; ++i) {
        host_buffer[i] = i;
    }

    // Create the device buffer
    cl_mem device_buffer = clCreateBuffer(
        context, CL_MEM_READ_WRITE, SAMPLE_SIZE * sizeof(int), 0, 0
    );

    // Set kernel arguments
    clSetKernelArg(kernel, 0, sizeof(cl_mem), (void *)&device_buffer);
    clSetKernelArg(kernel, 1, sizeof(int), (void *)&SAMPLE_SIZE);

    // Create the command queue
    cl_command_queue command_queue =
        clCreateCommandQueue(context, device_id, 0, 0);

    // Host buffer -> Device buffer
    clEnqueueWriteBuffer(
        command_queue, device_buffer, CL_FALSE, 0, SAMPLE_SIZE * sizeof(int),
        host_buffer, 0, 0, 0
    );

    // Size specification
    size_t local_work_size = 256;
    size_t n_work_groups =
        (SAMPLE_SIZE + local_work_size + 1) / local_work_size;
    size_t global_work_size = n_work_groups * local_work_size;

    // Apply the kernel on the range
    clEnqueueNDRangeKernel(
        command_queue, kernel, 1, 0, &global_work_size, &local_work_size, 0, 0,
        0
    );

    // Host buffer <- Device buffer
    clEnqueueReadBuffer(
        command_queue, device_buffer, CL_TRUE, 0, SAMPLE_SIZE * sizeof(int),
        host_buffer, 0, 0, 0
    );

    for (i = 0; i < SAMPLE_SIZE; ++i) {
        printf("[%d] = %d, ", i, host_buffer[i]);
    }

    // Release the resources
    clReleaseKernel(kernel);
    clReleaseProgram(program);
    clReleaseContext(context);
    clReleaseDevice(device_id);

    free(host_buffer);
}
