#define CL_TARGET_OPENCL_VERSION 220
#include <CL/cl.h>

#include <stdio.h>
#include <stdlib.h>

#include "mycl.h"
#include "mycutil.h"

const int SAMPLE_SIZE = 1000;

void CL_CALLBACK write_complete(cl_event e, cl_int status, void *data) {
    printf("[CB] %s", (char *)data);
}

void CL_CALLBACK read_complete(cl_event e, cl_int status, void *data) {
    printf("[CB] %s", (char *)data);
}

int main(void) {
    CLContext context;
    CLKernel kernel;
    cl_int err;
    bool success;
    int i;

    // Load OpenCL environment
    context = get_cl_context(&success);
    if (!success) {
        return EXIT_FAILURE;
    }

    // Load kernel
    kernel = load_cl_kernel(context, "kernels/sample.cl", &success);

    // Create the host buffer and initialize it
    int *host_buffer = (int *)malloc(SAMPLE_SIZE * sizeof(int));
    for (i = 0; i < SAMPLE_SIZE; ++i) {
        host_buffer[i] = i;
    }

    // Create the device buffer
    cl_mem device_buffer = clCreateBuffer(
        context.context, CL_MEM_READ_WRITE | CL_MEM_USE_HOST_PTR,
        SAMPLE_SIZE * sizeof(int), host_buffer, &err
    );
    if (err != CL_SUCCESS) {
        printf("Unable to create buffer! Code: %d\n", err);
        return EXIT_FAILURE;
    }

    // Set kernel arguments
    clSetKernelArg(kernel.kernel, 0, sizeof(cl_mem), (void *)&device_buffer);
    clSetKernelArg(kernel.kernel, 1, sizeof(int), (void *)&SAMPLE_SIZE);

    // Create the command queue
    cl_command_queue command_queue = clCreateCommandQueueWithProperties(
        context.context, context.device_id, NULL, NULL
    );

    // Host buffer -> Device buffer
    cl_event write_event;
    printf("Enqueue writing to the buffer ...\n");
    clEnqueueWriteBuffer(
        command_queue, device_buffer, CL_FALSE, 0, SAMPLE_SIZE * sizeof(int),
        host_buffer, 0, NULL, &write_event
    );
    printf("Buffer writing is on the queue!\n");

    char write_msg[] = "Write operation is ready!\n";
    err = clSetEventCallback(
        write_event, CL_COMPLETE, &write_complete, write_msg
    );
    if (err != CL_SUCCESS) {
        printf("Unable to create the event callback! Code: %d\n", err);
        return EXIT_FAILURE;
    }

    // Size specification
    size_t local_work_size = 256;
    size_t n_work_groups =
        (SAMPLE_SIZE + local_work_size + 1) / local_work_size;
    size_t global_work_size = n_work_groups * local_work_size;

    // Apply the kernel on the range
    clEnqueueNDRangeKernel(
        command_queue, kernel.kernel, 1, NULL, &global_work_size,
        &local_work_size, 0, NULL, NULL
    );

    // Host buffer <- Device buffer
    cl_event read_event;
    printf("Enqueue reading from the buffer ...\n");
    clEnqueueReadBuffer(
        command_queue, device_buffer, CL_TRUE, 0, SAMPLE_SIZE * sizeof(int),
        host_buffer, 0, NULL, &read_event
    );
    printf("Buffer reading is on the queue!\n");

    char read_msg[] = "Read operation is ready!\n";
    err = clSetEventCallback(read_event, CL_COMPLETE, &read_complete, read_msg);
    if (err != CL_SUCCESS) {
        printf("Unable to create the event callback! Code: %d\n", err);
        return EXIT_FAILURE;
    }

    clFinish(command_queue);

    for (i = 0; i < SAMPLE_SIZE; ++i) {
        // printf("[%d] = %d, ", i, host_buffer[i]);
    }

    // Release the resources
    printf("Release resources ...\n");
    clReleaseEvent(write_event);
    clReleaseEvent(read_event);
    clReleaseMemObject(device_buffer);
    release_cl_kernel(kernel);
    release_cl_context(context);

    free(host_buffer);

    printf("Ready!\n");
    return EXIT_SUCCESS;
}
