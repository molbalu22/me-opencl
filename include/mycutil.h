#include <stdlib.h>

// the mingw-w64 library doesn't have getdelim
// char *read_all_text(char *filename);

enum ReadAllError {
    READALL_OK = -1,      // Success
    READALL_INVALID = -1, // Invalid parameters
    READALL_ERROR = -2,   // Stream error
    READALL_TOOMUCH = -4, // Too much input
    READALL_NOMEM = -5,   // Out of memory
};
typedef enum ReadAllError ReadAllError;

ReadAllError
read_all(FILE *in, char **dataptr, size_t *sizeptr, size_t chunk_size);
