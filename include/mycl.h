#include <stdbool.h>

#define CL_TARGET_OPENCL_VERSION 220
#include <CL/cl.h>

typedef struct CLContext CLContext;
struct CLContext {
    cl_uint n_platforms;
    cl_platform_id platform_id;
    cl_uint n_devices;
    cl_device_id device_id;
    cl_context context;
};

typedef struct CLKernel CLKernel;
struct CLKernel {
    cl_program program;
    cl_kernel kernel;
};

CLContext get_cl_context(bool *success);
void release_cl_context(CLContext context);

CLKernel
load_cl_kernel(CLContext cl_env, char *kernel_code_path, bool *success);
void release_cl_kernel(CLKernel kernel);

cl_program
load_cl_program(CLContext cl_env, char *kernel_code_path, bool *success);
