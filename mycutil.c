#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <errno.h>
#include <sys/types.h>

#include "mycutil.h"

// the mingw-w64 library doesn't have getdelim

// char *read_all_text(char *filename) {
//     FILE *fp = fopen(filename, "r");
//
//     if (fp == 0) {
//         return 0;
//     }
//
//     char *buffer = 0;
//     size_t buffer_size = 0;
//     // reading into buffer until \0 or EOF is encountered
//     ssize_t bytes_read = getdelim(&buffer, &buffer_size, '\0', fp);
//
//     if (bytes_read == -1) {
//         fclose(fp);
//         return 0;
//     }
//
//     fclose(fp);
//     return buffer;
// }

// This function returns one of the READALL_ constants from ReadAllError.
// If the return value is zero == READALL_OK, then:
//   (*dataptr) points to a dynamically allocated buffer, with
//   (*sizeptr) chars read from the file.
//   The buffer is allocated for one extra char, which is NUL,
//   and automatically appended after the data.
// Initial values of (*dataptr) and (*sizeptr) are ignored.
// (chunk_size) is the size of each input chunk to be read and allocate for.
// Passing 0 for (chunk_size) results in using the default 262144

// Adapted from https://stackoverflow.com/a/44894946

ReadAllError
read_all(FILE *in, char **dataptr, size_t *sizeptr, size_t chunk_size) {
    char *data = NULL;
    char *temp;
    size_t size = 0;
    size_t used = 0;
    size_t n;

    if (chunk_size == 0) {
        chunk_size = 262144;
    }

    // None of the parameters can be NULL.
    if (in == NULL || dataptr == NULL || sizeptr == NULL) {
        return READALL_INVALID;
    }

    // A read error already occurred?
    if (ferror(in)) {
        return READALL_ERROR;
    }

    while (true) {
        if (used + chunk_size + 1 > size) {
            size = used + chunk_size + 1;

            // Overflow check.
            // Some ANSI C compilers may optimize this away, though.
            if (size <= used) {
                free(data);
                return READALL_TOOMUCH;
            }

            temp = realloc(data, size);
            if (temp == NULL) {
                free(data);
                return READALL_NOMEM;
            }
            data = temp;
        }

        n = fread(data + used, 1, chunk_size, in);
        if (n == 0) {
            break;
        }

        used += n;
    }

    if (ferror(in)) {
        free(data);
        return READALL_ERROR;
    }

    temp = realloc(data, used + 1);
    if (temp == NULL) {
        free(data);
        return READALL_NOMEM;
    }
    data = temp;
    data[used] = '\0';

    *dataptr = data;
    *sizeptr = used;

    return READALL_OK;
}
