#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>

#define CL_TARGET_OPENCL_VERSION 220
#include <CL/cl.h>

#include "mycutil.h"

#include "mycl.h"

CLContext get_cl_context(bool *success) {
    cl_int err;
    CLContext context;

    // Get platform
    err = clGetPlatformIDs(1, &context.platform_id, &context.n_platforms);
    if (err != CL_SUCCESS) {
        printf("[ERROR] Error calling clGetPlatformIDs. Error code: %d\n", err);
        *success = false;
        return context;
    }

    // Get device
    err = clGetDeviceIDs(
        context.platform_id, CL_DEVICE_TYPE_GPU, 1, &context.device_id,
        &context.n_devices
    );
    if (err != CL_SUCCESS) {
        printf("[ERROR] Error calling clGetDeviceIDs. Error code: %d\n", err);
        *success = false;
        return context;
    }

    // Create OpenCL context
    context.context = clCreateContext(
        NULL, context.n_devices, &context.device_id, NULL, NULL, NULL
    );

    *success = true;
    return context;
}

void release_cl_context(CLContext context) {
    clReleaseContext(context.context);
    clReleaseDevice(context.device_id);
}

CLKernel
load_cl_kernel(CLContext cl_env, char *kernel_code_path, bool *success) {
    cl_int err;
    CLKernel kernel = {0};

    char *kernel_code;
    size_t kernel_code_size;
    FILE *fp = fopen(kernel_code_path, "r");

    if (fp == 0) {
        *success = false;
        return kernel;
    }

    ReadAllError error = read_all(fp, &kernel_code, &kernel_code_size, 0);
    fclose(fp);

    if (error != READALL_OK) {
        perror("kernel code not loaded");
        *success = false;
        return kernel;
    }

    kernel.program = clCreateProgramWithSource(
        cl_env.context, 1, (char const **)&kernel_code, NULL, NULL
    );
    const char options[] = "";
    err = clBuildProgram(
        kernel.program, 1, &cl_env.device_id, options, NULL, NULL
    );
    if (err != CL_SUCCESS) {
        printf("Build error! Code: %d\n", err);
        size_t real_size;
        err = clGetProgramBuildInfo(
            kernel.program, cl_env.device_id, CL_PROGRAM_BUILD_LOG, 0, NULL,
            &real_size
        );
        char *build_log = (char *)malloc(sizeof(char) * (real_size + 1));
        err = clGetProgramBuildInfo(
            kernel.program, cl_env.device_id, CL_PROGRAM_BUILD_LOG,
            real_size + 1, build_log, &real_size
        );
        printf("Real size : %lu\n", (unsigned long)real_size);
        printf("Build log : %s\n", build_log);
        free(build_log);
        *success = false;
        return kernel;
    }
    kernel.kernel = clCreateKernel(kernel.program, "sample_kernel", NULL);

    *success = true;
    return kernel;
}

void release_cl_kernel(CLKernel kernel) {
    clReleaseKernel(kernel.kernel);
    clReleaseProgram(kernel.program);
}

cl_program
load_cl_program(CLContext cl_env, char *kernel_code_path, bool *success) {
    cl_int err;
    cl_program program = 0;

    char *kernel_code;
    size_t kernel_code_size;
    FILE *fp = fopen(kernel_code_path, "r");

    if (fp == 0) {
        *success = false;
        return program;
    }

    ReadAllError error = read_all(fp, &kernel_code, &kernel_code_size, 0);
    fclose(fp);

    if (error != READALL_OK) {
        perror("kernel code not loaded");
        *success = false;
        return program;
    }

    program = clCreateProgramWithSource(
        cl_env.context, 1, (char const **)&kernel_code, NULL, NULL
    );
    const char options[] = "";
    err = clBuildProgram(program, 1, &cl_env.device_id, options, NULL, NULL);
    if (err != CL_SUCCESS) {
        printf("Build error! Code: %d\n", err);
        size_t real_size;
        err = clGetProgramBuildInfo(
            program, cl_env.device_id, CL_PROGRAM_BUILD_LOG, 0, NULL, &real_size
        );
        char *build_log = (char *)malloc(sizeof(char) * (real_size + 1));
        err = clGetProgramBuildInfo(
            program, cl_env.device_id, CL_PROGRAM_BUILD_LOG, real_size + 1,
            build_log, &real_size
        );
        printf("Real size : %lu\n", (unsigned long)real_size);
        printf("Build log : %s\n", build_log);
        free(build_log);
        *success = false;
        return program;
    }

    *success = true;
    return program;
}
