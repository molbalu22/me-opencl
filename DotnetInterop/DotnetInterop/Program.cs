﻿using System.Globalization;
using DotnetInterop;

CultureInfo.CurrentCulture = CultureInfo.InvariantCulture;

const int SUCCESS = 0;

// const int FAILURE = 1;

int status = Interop.MountClLib();

if (status != SUCCESS)
{
    throw new Exception("Unable to load OpenCL");
}

void WriteTitle(string title)
{
    int length = title.Length;
    Console.WriteLine();
    Console.WriteLine(title);
    Console.WriteLine(new string('=', length));
}


{
    WriteTitle(nameof(Interop.TestArrays));
    int[] buf = { 1, 2, 3 };
    Interop.TestArrays(buf);

    foreach (int item in buf)
    {
        Console.WriteLine(item);
    }
}


{
    WriteTitle(nameof(Interop.TestCl));
    int[] buf = { 4, 5, 6, 7 };
    status = Interop.TestCl(buf);

    if (status != SUCCESS)
    {
        throw new Exception("Unable to load demo kernel");
    }

    foreach (int item in buf)
    {
        Console.WriteLine(item);
    }
}


{
    WriteTitle(nameof(Interop.MatrixMultiply));
    float[,] a =
    {
        { 1, 2 },
        { 3, 4 },
        { 5, 6 }
    };
    float[,] b =
    {
        { 2, 4, 6 },
        { 3, 5, 7 }
    };
    float[,] c = Interop.MatrixMultiply(a, b);
    Console.WriteLine(string.Join(" ", c.Cast<float>()));
}


{
    WriteTitle(nameof(Interop.MatrixTransposeAndMultiply));
    float[,] a =
    {
        { 1, 3, 5 },
        { 2, 4, 6 }
    };
    float[,] b =
    {
        { 2, 4, 6 },
        { 3, 5, 7 }
    };
    float[,] c = Interop.MatrixTransposeAndMultiply(a, b);
    Console.WriteLine(string.Join(" ", c.Cast<float>()));
}


{
    WriteTitle(nameof(Interop.VectorAdd));
    float[] a = { 5.1f, 6.1f, 7.1f };
    float[] b = { 7.1f, 8.1f, 9.1f };
    float[] c = Interop.VectorAdd(a, b);
    Console.WriteLine(
        string.Join(" ", Array.ConvertAll(c, i => i.ToString(CultureInfo.InvariantCulture)))
    );
}


{
    WriteTitle(nameof(Interop.MatrixAdd));
    float[,] a =
    {
        { 2.2f, 3.2f },
        { 4.2f, 5.2f },
        { 6.2f, 7.2f }
    };
    float[,] b =
    {
        { 4.3f, 5.3f },
        { 6.3f, 7.3f },
        { 8.3f, 9.3f }
    };
    float[,] c = Interop.MatrixAdd(a, b);
    Console.WriteLine(string.Join(" ", c.Cast<float>()));
}


{
    WriteTitle(nameof(Interop.VectorSubtract));
    float[] a = { 5.1f, 6.1f, 7.1f };
    float[] b = { 9.1f, 8.1f, 7.1f };
    float[] c = Interop.VectorSubtract(a, b);
    Console.WriteLine(
        string.Join(" ", Array.ConvertAll(c, i => i.ToString(CultureInfo.InvariantCulture)))
    );
}


{
    WriteTitle(nameof(Interop.MatrixSubtract));
    float[,] a =
    {
        { 2.2f, 3.2f },
        { 4.2f, 5.2f },
        { 6.2f, 7.2f }
    };
    float[,] b =
    {
        { 9.3f, 8.3f },
        { 7.3f, 6.3f },
        { 5.3f, 4.3f }
    };
    float[,] c = Interop.MatrixSubtract(a, b);
    Console.WriteLine(string.Join(" ", c.Cast<float>()));
}


{
    WriteTitle(nameof(Interop.VectorHadamardProduct));
    float[] a = { 0.1f, 0.2f, 0.3f };
    float[] b = { 0.2f, 0.3f, 0.4f };
    float[] c = Interop.VectorHadamardProduct(a, b);
    Console.WriteLine(
        string.Join(" ", Array.ConvertAll(c, i => i.ToString(CultureInfo.InvariantCulture)))
    );
}


{
    WriteTitle(nameof(Interop.MatrixHadamardProduct));
    float[,] a =
    {
        { 0.1f, 0.2f },
        { 0.3f, 0.4f },
        { 0.5f, 0.6f }
    };
    float[,] b =
    {
        { 0.3f, 0.4f },
        { 0.5f, 0.6f },
        { 0.7f, 0.8f }
    };
    float[,] c = Interop.MatrixHadamardProduct(a, b);
    Console.WriteLine(string.Join(" ", c.Cast<float>()));
}

Interop.UnmountClLib();
