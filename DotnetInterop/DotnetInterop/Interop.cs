﻿using System.Diagnostics.CodeAnalysis;
using System.Runtime.InteropServices;

namespace DotnetInterop;

public static class Interop
{
    private const int SUCCESS = 0;

    // private const int FAILURE = 1;

    [SuppressMessage("ReSharper", "MemberHidesStaticFromOuterClass")]
    private static class Lib
    {
        [DllImport("libclinterop.dll")]
        public static extern int MountClLib();

        [DllImport("libclinterop.dll")]
        public static extern void UnmountClLib();

        [DllImport("libclinterop.dll")]
        public static extern void TestArrays(int[] buf);

        [DllImport("libclinterop.dll")]
        public static extern int TestCl(int[] buf, int bufSize);

        [DllImport("libclinterop.dll")]
        public static extern int MatrixMultiply(
            int rows,
            int cols,
            int n,
            float[,] a,
            float[,] b,
            float[,] c
        );

        [DllImport("libclinterop.dll")]
        public static extern int MatrixTransposeAndMultiply(
            int rows,
            int cols,
            int n,
            float[,] a,
            float[,] b,
            float[,] c
        );

        [DllImport("libclinterop.dll")]
        public static extern int VectorAdd(int len, float[] a, float[] b, float[] c);

        [DllImport("libclinterop.dll")]
        public static extern int MatrixAdd(int rows, int cols, float[,] a, float[,] b, float[,] c);

        [DllImport("libclinterop.dll")]
        public static extern int VectorSubtract(int len, float[] a, float[] b, float[] c);

        [DllImport("libclinterop.dll")]
        public static extern int MatrixSubtract(
            int rows,
            int cols,
            float[,] a,
            float[,] b,
            float[,] c
        );

        [DllImport("libclinterop.dll")]
        public static extern int VectorHadamardProduct(int len, float[] a, float[] b, float[] c);

        [DllImport("libclinterop.dll")]
        public static extern int MatrixHadamardProduct(
            int rows,
            int cols,
            float[,] a,
            float[,] b,
            float[,] c
        );
    }

    public static int MountClLib()
    {
        return Lib.MountClLib();
    }

    public static void UnmountClLib()
    {
        Lib.UnmountClLib();
    }

    public static void TestArrays(int[] buf)
    {
        Lib.TestArrays(buf);
    }

    public static int TestCl(int[] buf)
    {
        return Lib.TestCl(buf, buf.Length);
    }

    public static float[,] MatrixMultiply(float[,] a, float[,] b)
    {
        int rows = a.GetLength(0);
        int n = a.GetLength(1);
        int cols = b.GetLength(1);

        float[,] c = new float[rows, cols];

        int status = Lib.MatrixMultiply(rows, cols, n, a, b, c);

        if (status != SUCCESS)
        {
            throw new Exception($"OpenCL {nameof(MatrixMultiply)} error");
        }

        return c;
    }

    public static float[,] MatrixTransposeAndMultiply(float[,] a, float[,] b)
    {
        int n = a.GetLength(0);
        int rows = a.GetLength(1);
        int cols = b.GetLength(1);

        float[,] c = new float[rows, cols];

        int status = Lib.MatrixTransposeAndMultiply(rows, cols, n, a, b, c);

        if (status != SUCCESS)
        {
            throw new Exception($"OpenCL {nameof(MatrixTransposeAndMultiply)} error");
        }

        return c;
    }

    public static float[] VectorAdd(float[] a, float[] b)
    {
        int len = a.Length;

        float[] c = new float[len];

        int status = Lib.VectorAdd(len, a, b, c);

        if (status != SUCCESS)
        {
            throw new Exception($"OpenCL {nameof(VectorAdd)} error");
        }

        return c;
    }

    public static float[,] MatrixAdd(float[,] a, float[,] b)
    {
        int rows = a.GetLength(0);
        int cols = a.GetLength(1);

        float[,] c = new float[rows, cols];

        int status = Lib.MatrixAdd(rows, cols, a, b, c);

        if (status != SUCCESS)
        {
            throw new Exception($"OpenCL {nameof(MatrixAdd)} error");
        }

        return c;
    }

    public static float[] VectorSubtract(float[] a, float[] b)
    {
        int len = a.Length;

        float[] c = new float[len];

        int status = Lib.VectorSubtract(len, a, b, c);

        if (status != SUCCESS)
        {
            throw new Exception($"OpenCL {nameof(VectorSubtract)} error");
        }

        return c;
    }

    public static float[,] MatrixSubtract(float[,] a, float[,] b)
    {
        int rows = a.GetLength(0);
        int cols = a.GetLength(1);

        float[,] c = new float[rows, cols];

        int status = Lib.MatrixSubtract(rows, cols, a, b, c);

        if (status != SUCCESS)
        {
            throw new Exception($"OpenCL {nameof(MatrixSubtract)} error");
        }

        return c;
    }

    public static float[] VectorHadamardProduct(float[] a, float[] b)
    {
        int len = a.Length;

        float[] c = new float[len];

        int status = Lib.VectorHadamardProduct(len, a, b, c);

        if (status != SUCCESS)
        {
            throw new Exception($"OpenCL {nameof(VectorHadamardProduct)} error");
        }

        return c;
    }

    public static float[,] MatrixHadamardProduct(float[,] a, float[,] b)
    {
        int rows = a.GetLength(0);
        int cols = a.GetLength(1);

        float[,] c = new float[rows, cols];

        int status = Lib.MatrixHadamardProduct(rows, cols, a, b, c);

        if (status != SUCCESS)
        {
            throw new Exception($"OpenCL {nameof(MatrixHadamardProduct)} error");
        }

        return c;
    }
}
