#define CL_TARGET_OPENCL_VERSION 220
#include <CL/cl.h>

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "mycl.h"

int const SUCCESS = 0;
int const FAILURE = 1;

CLContext lib_global_cl_context;

struct LibCLKernels {
    bool demo_kernel_loaded;
    cl_program demo_kernel_program;
    bool matrix_kernel_loaded;
    cl_program matrix_kernel_program;
    bool vector_kernel_loaded;
    cl_program vector_kernel_program;
} lib_global_cl_kernels = {0};

int MountClLib(void) {
    bool success;

    // Load OpenCL context
    lib_global_cl_context = get_cl_context(&success);
    if (!success) {
        return FAILURE;
    }

#ifdef FP_64
    printf("double support available\n");
#else
    printf("double support isn't available\n");
#endif

    return SUCCESS;
}

void UnmountClLib(void) { release_cl_context(lib_global_cl_context); }

void TestArrays(int *buf) {
    buf[0] = 111;
    buf[1] = 222;
}

int TestCl(int *buf, int bufSize) {
    if (!lib_global_cl_kernels.demo_kernel_loaded) {
        bool success;
        lib_global_cl_kernels.demo_kernel_program =
            load_cl_program(lib_global_cl_context, "demo_kernel.cl", &success);

        if (!success) {
            return FAILURE;
        } else {
            lib_global_cl_kernels.demo_kernel_loaded = true;
        }
    }

    // Create a kernel for this call
    cl_kernel kernel = clCreateKernel(
        lib_global_cl_kernels.demo_kernel_program, "demo_kernel", 0
    );

    // Create the device buffer
    cl_mem device_buffer = clCreateBuffer(
        lib_global_cl_context.context, CL_MEM_READ_WRITE, bufSize * sizeof(int),
        0, 0
    );

    // Set kernel arguments
    clSetKernelArg(kernel, 0, sizeof(cl_mem), (void *)&device_buffer);
    clSetKernelArg(kernel, 1, sizeof(int), (void *)&bufSize);

    // Create the command queue
    cl_command_queue command_queue = clCreateCommandQueueWithProperties(
        lib_global_cl_context.context, lib_global_cl_context.device_id, 0, 0
    );

    // Host buffer -> Device buffer
    clEnqueueWriteBuffer(
        command_queue, device_buffer, CL_FALSE, 0, bufSize * sizeof(int), buf,
        0, 0, 0
    );

    // Size specification
    size_t local_work_size = 256;
    size_t n_work_groups = (bufSize + local_work_size + 1) / local_work_size;
    size_t global_work_size = n_work_groups * local_work_size;

    // Apply the kernel on the range
    clEnqueueNDRangeKernel(
        command_queue, kernel, 1, 0, &global_work_size, &local_work_size, 0, 0,
        0
    );

    // Host buffer <- Device buffer
    clEnqueueReadBuffer(
        command_queue, device_buffer, CL_TRUE, 0, bufSize * sizeof(int), buf, 0,
        0, 0
    );

    clFinish(command_queue);

    return SUCCESS;
}

int MatrixMultiply(int rows, int cols, int N, float *A, float *B, float *C) {
    if (!lib_global_cl_kernels.matrix_kernel_loaded) {
        bool success;
        lib_global_cl_kernels.matrix_kernel_program = load_cl_program(
            lib_global_cl_context, "matrix_kernel.cl", &success
        );

        if (!success) {
            return FAILURE;
        } else {
            lib_global_cl_kernels.matrix_kernel_loaded = true;
        }
    }

    cl_int status;

    // Create a kernel for this call
    cl_kernel kernel = clCreateKernel(
        lib_global_cl_kernels.matrix_kernel_program, "multiply_kernel", &status
    );

    printf("CreateKernel status: %d\n", status);

    // Create the device buffers
    cl_mem A_buffer = clCreateBuffer(
        lib_global_cl_context.context, CL_MEM_READ_WRITE,
        rows * N * sizeof(float), 0, 0
    );
    cl_mem B_buffer = clCreateBuffer(
        lib_global_cl_context.context, CL_MEM_READ_WRITE,
        N * cols * sizeof(float), 0, 0
    );
    cl_mem C_buffer = clCreateBuffer(
        lib_global_cl_context.context, CL_MEM_READ_WRITE,
        rows * cols * sizeof(float), 0, 0
    );

    // Set kernel arguments
    clSetKernelArg(kernel, 0, sizeof(int), (void *)&rows);
    clSetKernelArg(kernel, 1, sizeof(int), (void *)&cols);
    clSetKernelArg(kernel, 2, sizeof(int), (void *)&N);
    clSetKernelArg(kernel, 3, sizeof(cl_mem), (void *)&A_buffer);
    clSetKernelArg(kernel, 4, sizeof(cl_mem), (void *)&B_buffer);
    clSetKernelArg(kernel, 5, sizeof(cl_mem), (void *)&C_buffer);

    // Create the command queue
    cl_command_queue command_queue = clCreateCommandQueueWithProperties(
        lib_global_cl_context.context, lib_global_cl_context.device_id, 0, 0
    );

    // Host buffer -> Device buffer
    clEnqueueWriteBuffer(
        command_queue, A_buffer, CL_FALSE, 0, rows * N * sizeof(float), A, 0, 0,
        0
    );
    clEnqueueWriteBuffer(
        command_queue, B_buffer, CL_FALSE, 0, N * cols * sizeof(float), B, 0, 0,
        0
    );
    clEnqueueWriteBuffer(
        command_queue, C_buffer, CL_FALSE, 0, rows * cols * sizeof(float), C, 0,
        0, 0
    );

    // Size specification
    size_t global_work_size[2] = {rows, cols};

    // Apply the kernel on the range
    status = clEnqueueNDRangeKernel(
        command_queue, kernel, 2, 0, global_work_size, 0, 0, 0, 0
    );

    printf("EnqueueNDRangeKernel status: %d\n", status);

    // Host buffer <- Device buffer
    clEnqueueReadBuffer(
        command_queue, C_buffer, CL_TRUE, 0, rows * cols * sizeof(float), C, 0,
        0, 0
    );

    clFinish(command_queue);

    return SUCCESS;
}

int MatrixTransposeAndMultiply(
    int rows, int cols, int N, float *A, float *B, float *C
) {
    if (!lib_global_cl_kernels.matrix_kernel_loaded) {
        bool success;
        lib_global_cl_kernels.matrix_kernel_program = load_cl_program(
            lib_global_cl_context, "matrix_kernel.cl", &success
        );

        if (!success) {
            return FAILURE;
        } else {
            lib_global_cl_kernels.matrix_kernel_loaded = true;
        }
    }

    cl_int status;

    // Create a kernel for this call
    cl_kernel kernel = clCreateKernel(
        lib_global_cl_kernels.matrix_kernel_program,
        "transpose_multiply_kernel", &status
    );

    // Create the device buffers
    cl_mem A_buffer = clCreateBuffer(
        lib_global_cl_context.context, CL_MEM_READ_ONLY,
        N * rows * sizeof(float), 0, 0
    );
    cl_mem B_buffer = clCreateBuffer(
        lib_global_cl_context.context, CL_MEM_READ_ONLY,
        N * cols * sizeof(float), 0, 0
    );
    cl_mem C_buffer = clCreateBuffer(
        lib_global_cl_context.context, CL_MEM_WRITE_ONLY,
        rows * cols * sizeof(float), 0, 0
    );

    // Set kernel arguments
    clSetKernelArg(kernel, 0, sizeof(int), (void *)&rows);
    clSetKernelArg(kernel, 1, sizeof(int), (void *)&cols);
    clSetKernelArg(kernel, 2, sizeof(int), (void *)&N);
    clSetKernelArg(kernel, 3, sizeof(cl_mem), (void *)&A_buffer);
    clSetKernelArg(kernel, 4, sizeof(cl_mem), (void *)&B_buffer);
    clSetKernelArg(kernel, 5, sizeof(cl_mem), (void *)&C_buffer);

    // Create the command queue

    // Properties:
    //   CL_​QUEUE_​PROPERTIES:
    //     - CL_​QUEUE_​PROFILING_​ENABLE
    cl_queue_properties properties[] = {
        CL_QUEUE_PROPERTIES, CL_QUEUE_PROFILING_ENABLE, 0};

    cl_command_queue command_queue = clCreateCommandQueueWithProperties(
        lib_global_cl_context.context, lib_global_cl_context.device_id,
        properties, &status
    );

    // Host buffer -> Device buffer
    cl_event buffer_write_start;
    cl_event buffer_write_end;
    clEnqueueWriteBuffer(
        command_queue, A_buffer, CL_FALSE, 0, N * rows * sizeof(float), A, 0, 0,
        &buffer_write_start
    );
    clEnqueueWriteBuffer(
        command_queue, B_buffer, CL_FALSE, 0, N * cols * sizeof(float), B, 0, 0,
        0
    );
    clEnqueueWriteBuffer(
        command_queue, C_buffer, CL_FALSE, 0, rows * cols * sizeof(float), C, 0,
        0, &buffer_write_end
    );

    // Size specification
    size_t global_work_size[2] = {rows, cols};

    // Apply the kernel on the range
    cl_event calculation;
    status = clEnqueueNDRangeKernel(
        command_queue, kernel, 2, 0, global_work_size, 0, 0, 0, &calculation
    );

    // Host buffer <- Device buffer
    cl_event buffer_read;
    clEnqueueReadBuffer(
        command_queue, C_buffer, CL_TRUE, 0, rows * cols * sizeof(float), C, 0,
        0, &buffer_read
    );

    clFinish(command_queue);

    cl_ulong buffer_write_start_time;
    cl_ulong buffer_write_end_time;
    cl_ulong calculation_start_time;
    cl_ulong calculation_end_time;
    cl_ulong buffer_read_start_time;
    cl_ulong buffer_read_end_time;

    status = clGetEventProfilingInfo(
        buffer_write_start, CL_PROFILING_COMMAND_START, sizeof(cl_ulong),
        &buffer_write_start_time, 0
    );
    status = clGetEventProfilingInfo(
        buffer_write_end, CL_PROFILING_COMMAND_END, sizeof(cl_ulong),
        &buffer_write_end_time, 0
    );
    status = clGetEventProfilingInfo(
        calculation, CL_PROFILING_COMMAND_START, sizeof(cl_ulong),
        &calculation_start_time, 0
    );
    status = clGetEventProfilingInfo(
        calculation, CL_PROFILING_COMMAND_END, sizeof(cl_ulong),
        &calculation_end_time, 0
    );
    status = clGetEventProfilingInfo(
        buffer_read, CL_PROFILING_COMMAND_START, sizeof(cl_ulong),
        &buffer_read_start_time, 0
    );
    status = clGetEventProfilingInfo(
        buffer_read, CL_PROFILING_COMMAND_END, sizeof(cl_ulong),
        &buffer_read_end_time, 0
    );

    clReleaseMemObject(A_buffer);
    clReleaseMemObject(B_buffer);
    clReleaseMemObject(C_buffer);

    unsigned long long buffer_write_time =
        (unsigned long long)(buffer_write_end_time - buffer_write_start_time);
    unsigned long long calculation_time =
        (unsigned long long)(calculation_end_time - calculation_start_time);
    unsigned long long buffer_read_time =
        (unsigned long long)(buffer_read_end_time - buffer_read_start_time);

    printf("Buffer write time: %lluns\n", buffer_write_time);
    printf("Calculation time: %lluns\n", calculation_time);
    printf("Buffer read time: %lluns\n", buffer_read_time);

    return SUCCESS;
}

int VectorAdd(int len, float *A, float *B, float *C) {
    if (!lib_global_cl_kernels.vector_kernel_loaded) {
        bool success;
        lib_global_cl_kernels.vector_kernel_program = load_cl_program(
            lib_global_cl_context, "vector_kernel.cl", &success
        );

        if (!success) {
            return FAILURE;
        } else {
            lib_global_cl_kernels.vector_kernel_loaded = true;
        }
    }

    cl_int status;

    // Create a kernel for this call
    cl_kernel kernel = clCreateKernel(
        lib_global_cl_kernels.vector_kernel_program, "add_kernel", &status
    );

    printf("CreateKernel status: %d\n", status);

    // Create the device buffers
    cl_mem A_buffer = clCreateBuffer(
        lib_global_cl_context.context, CL_MEM_READ_WRITE, len * sizeof(float),
        0, 0
    );
    cl_mem B_buffer = clCreateBuffer(
        lib_global_cl_context.context, CL_MEM_READ_WRITE, len * sizeof(float),
        0, 0
    );
    cl_mem C_buffer = clCreateBuffer(
        lib_global_cl_context.context, CL_MEM_READ_WRITE, len * sizeof(float),
        0, 0
    );

    // Set kernel arguments
    clSetKernelArg(kernel, 0, sizeof(cl_mem), (void *)&A_buffer);
    clSetKernelArg(kernel, 1, sizeof(cl_mem), (void *)&B_buffer);
    clSetKernelArg(kernel, 2, sizeof(cl_mem), (void *)&C_buffer);

    // Create the command queue
    cl_command_queue command_queue = clCreateCommandQueueWithProperties(
        lib_global_cl_context.context, lib_global_cl_context.device_id, 0, 0
    );

    // Host buffer -> Device buffer
    clEnqueueWriteBuffer(
        command_queue, A_buffer, CL_FALSE, 0, len * sizeof(float), A, 0, 0, 0
    );
    clEnqueueWriteBuffer(
        command_queue, B_buffer, CL_FALSE, 0, len * sizeof(float), B, 0, 0, 0
    );
    clEnqueueWriteBuffer(
        command_queue, C_buffer, CL_FALSE, 0, len * sizeof(float), C, 0, 0, 0
    );

    // Size specification
    size_t global_work_size = len;

    // Apply the kernel on the range
    status = clEnqueueNDRangeKernel(
        command_queue, kernel, 1, 0, &global_work_size, 0, 0, 0, 0
    );

    printf("EnqueueNDRangeKernel status: %d\n", status);

    // Host buffer <- Device buffer
    clEnqueueReadBuffer(
        command_queue, C_buffer, CL_TRUE, 0, len * sizeof(float), C, 0, 0, 0
    );

    clFinish(command_queue);

    return SUCCESS;
}

int MatrixAdd(int rows, int cols, float *A, float *B, float *C) {
    if (!lib_global_cl_kernels.vector_kernel_loaded) {
        bool success;
        lib_global_cl_kernels.vector_kernel_program = load_cl_program(
            lib_global_cl_context, "vector_kernel.cl", &success
        );

        if (!success) {
            return FAILURE;
        } else {
            lib_global_cl_kernels.vector_kernel_loaded = true;
        }
    }

    cl_int status;

    // Create a kernel for this call
    cl_kernel kernel = clCreateKernel(
        lib_global_cl_kernels.vector_kernel_program, "add_kernel", &status
    );

    printf("CreateKernel status: %d\n", status);

    // Create the device buffers
    cl_mem A_buffer = clCreateBuffer(
        lib_global_cl_context.context, CL_MEM_READ_WRITE,
        rows * cols * sizeof(float), 0, 0
    );
    cl_mem B_buffer = clCreateBuffer(
        lib_global_cl_context.context, CL_MEM_READ_WRITE,
        rows * cols * sizeof(float), 0, 0
    );
    cl_mem C_buffer = clCreateBuffer(
        lib_global_cl_context.context, CL_MEM_READ_WRITE,
        rows * cols * sizeof(float), 0, 0
    );

    // Set kernel arguments
    clSetKernelArg(kernel, 0, sizeof(cl_mem), (void *)&A_buffer);
    clSetKernelArg(kernel, 1, sizeof(cl_mem), (void *)&B_buffer);
    clSetKernelArg(kernel, 2, sizeof(cl_mem), (void *)&C_buffer);

    // Create the command queue
    cl_command_queue command_queue = clCreateCommandQueueWithProperties(
        lib_global_cl_context.context, lib_global_cl_context.device_id, 0, 0
    );

    // Host buffer -> Device buffer
    clEnqueueWriteBuffer(
        command_queue, A_buffer, CL_FALSE, 0, rows * cols * sizeof(float), A, 0,
        0, 0
    );
    clEnqueueWriteBuffer(
        command_queue, B_buffer, CL_FALSE, 0, rows * cols * sizeof(float), B, 0,
        0, 0
    );
    clEnqueueWriteBuffer(
        command_queue, C_buffer, CL_FALSE, 0, rows * cols * sizeof(float), C, 0,
        0, 0
    );

    // Size specification
    size_t global_work_size = rows * cols;

    // Apply the kernel on the range
    status = clEnqueueNDRangeKernel(
        command_queue, kernel, 1, 0, &global_work_size, 0, 0, 0, 0
    );

    printf("EnqueueNDRangeKernel status: %d\n", status);

    // Host buffer <- Device buffer
    clEnqueueReadBuffer(
        command_queue, C_buffer, CL_TRUE, 0, rows * cols * sizeof(float), C, 0,
        0, 0
    );

    clFinish(command_queue);

    return SUCCESS;
}

int VectorSubtract(int len, float *A, float *B, float *C) {
    if (!lib_global_cl_kernels.vector_kernel_loaded) {
        bool success;
        lib_global_cl_kernels.vector_kernel_program = load_cl_program(
            lib_global_cl_context, "vector_kernel.cl", &success
        );

        if (!success) {
            return FAILURE;
        } else {
            lib_global_cl_kernels.vector_kernel_loaded = true;
        }
    }

    cl_int status;

    // Create a kernel for this call
    cl_kernel kernel = clCreateKernel(
        lib_global_cl_kernels.vector_kernel_program, "subtract_kernel", &status
    );

    printf("CreateKernel status: %d\n", status);

    // Create the device buffers
    cl_mem A_buffer = clCreateBuffer(
        lib_global_cl_context.context, CL_MEM_READ_WRITE, len * sizeof(float),
        0, 0
    );
    cl_mem B_buffer = clCreateBuffer(
        lib_global_cl_context.context, CL_MEM_READ_WRITE, len * sizeof(float),
        0, 0
    );
    cl_mem C_buffer = clCreateBuffer(
        lib_global_cl_context.context, CL_MEM_READ_WRITE, len * sizeof(float),
        0, 0
    );

    // Set kernel arguments
    clSetKernelArg(kernel, 0, sizeof(cl_mem), (void *)&A_buffer);
    clSetKernelArg(kernel, 1, sizeof(cl_mem), (void *)&B_buffer);
    clSetKernelArg(kernel, 2, sizeof(cl_mem), (void *)&C_buffer);

    // Create the command queue
    cl_command_queue command_queue = clCreateCommandQueueWithProperties(
        lib_global_cl_context.context, lib_global_cl_context.device_id, 0, 0
    );

    // Host buffer -> Device buffer
    clEnqueueWriteBuffer(
        command_queue, A_buffer, CL_FALSE, 0, len * sizeof(float), A, 0, 0, 0
    );
    clEnqueueWriteBuffer(
        command_queue, B_buffer, CL_FALSE, 0, len * sizeof(float), B, 0, 0, 0
    );
    clEnqueueWriteBuffer(
        command_queue, C_buffer, CL_FALSE, 0, len * sizeof(float), C, 0, 0, 0
    );

    // Size specification
    size_t global_work_size = len;

    // Apply the kernel on the range
    status = clEnqueueNDRangeKernel(
        command_queue, kernel, 1, 0, &global_work_size, 0, 0, 0, 0
    );

    printf("EnqueueNDRangeKernel status: %d\n", status);

    // Host buffer <- Device buffer
    clEnqueueReadBuffer(
        command_queue, C_buffer, CL_TRUE, 0, len * sizeof(float), C, 0, 0, 0
    );

    clFinish(command_queue);

    return SUCCESS;
}

int MatrixSubtract(int rows, int cols, float *A, float *B, float *C) {
    if (!lib_global_cl_kernels.vector_kernel_loaded) {
        bool success;
        lib_global_cl_kernels.vector_kernel_program = load_cl_program(
            lib_global_cl_context, "vector_kernel.cl", &success
        );

        if (!success) {
            return FAILURE;
        } else {
            lib_global_cl_kernels.vector_kernel_loaded = true;
        }
    }

    cl_int status;

    // Create a kernel for this call
    cl_kernel kernel = clCreateKernel(
        lib_global_cl_kernels.vector_kernel_program, "subtract_kernel", &status
    );

    printf("CreateKernel status: %d\n", status);

    // Create the device buffers
    cl_mem A_buffer = clCreateBuffer(
        lib_global_cl_context.context, CL_MEM_READ_WRITE,
        rows * cols * sizeof(float), 0, 0
    );
    cl_mem B_buffer = clCreateBuffer(
        lib_global_cl_context.context, CL_MEM_READ_WRITE,
        rows * cols * sizeof(float), 0, 0
    );
    cl_mem C_buffer = clCreateBuffer(
        lib_global_cl_context.context, CL_MEM_READ_WRITE,
        rows * cols * sizeof(float), 0, 0
    );

    // Set kernel arguments
    clSetKernelArg(kernel, 0, sizeof(cl_mem), (void *)&A_buffer);
    clSetKernelArg(kernel, 1, sizeof(cl_mem), (void *)&B_buffer);
    clSetKernelArg(kernel, 2, sizeof(cl_mem), (void *)&C_buffer);

    // Create the command queue
    cl_command_queue command_queue = clCreateCommandQueueWithProperties(
        lib_global_cl_context.context, lib_global_cl_context.device_id, 0, 0
    );

    // Host buffer -> Device buffer
    clEnqueueWriteBuffer(
        command_queue, A_buffer, CL_FALSE, 0, rows * cols * sizeof(float), A, 0,
        0, 0
    );
    clEnqueueWriteBuffer(
        command_queue, B_buffer, CL_FALSE, 0, rows * cols * sizeof(float), B, 0,
        0, 0
    );
    clEnqueueWriteBuffer(
        command_queue, C_buffer, CL_FALSE, 0, rows * cols * sizeof(float), C, 0,
        0, 0
    );

    // Size specification
    size_t global_work_size = rows * cols;

    // Apply the kernel on the range
    status = clEnqueueNDRangeKernel(
        command_queue, kernel, 1, 0, &global_work_size, 0, 0, 0, 0
    );

    printf("EnqueueNDRangeKernel status: %d\n", status);

    // Host buffer <- Device buffer
    clEnqueueReadBuffer(
        command_queue, C_buffer, CL_TRUE, 0, rows * cols * sizeof(float), C, 0,
        0, 0
    );

    clFinish(command_queue);

    return SUCCESS;
}

int VectorHadamardProduct(int len, float *A, float *B, float *C) {
    if (!lib_global_cl_kernels.vector_kernel_loaded) {
        bool success;
        lib_global_cl_kernels.vector_kernel_program = load_cl_program(
            lib_global_cl_context, "vector_kernel.cl", &success
        );

        if (!success) {
            return FAILURE;
        } else {
            lib_global_cl_kernels.vector_kernel_loaded = true;
        }
    }

    cl_int status;

    // Create a kernel for this call
    cl_kernel kernel = clCreateKernel(
        lib_global_cl_kernels.vector_kernel_program, "hadamard_kernel", &status
    );

    printf("CreateKernel status: %d\n", status);

    // Create the device buffers
    cl_mem A_buffer = clCreateBuffer(
        lib_global_cl_context.context, CL_MEM_READ_WRITE, len * sizeof(float),
        0, 0
    );
    cl_mem B_buffer = clCreateBuffer(
        lib_global_cl_context.context, CL_MEM_READ_WRITE, len * sizeof(float),
        0, 0
    );
    cl_mem C_buffer = clCreateBuffer(
        lib_global_cl_context.context, CL_MEM_READ_WRITE, len * sizeof(float),
        0, 0
    );

    // Set kernel arguments
    clSetKernelArg(kernel, 0, sizeof(cl_mem), (void *)&A_buffer);
    clSetKernelArg(kernel, 1, sizeof(cl_mem), (void *)&B_buffer);
    clSetKernelArg(kernel, 2, sizeof(cl_mem), (void *)&C_buffer);

    // Create the command queue
    cl_command_queue command_queue = clCreateCommandQueueWithProperties(
        lib_global_cl_context.context, lib_global_cl_context.device_id, 0, 0
    );

    // Host buffer -> Device buffer
    clEnqueueWriteBuffer(
        command_queue, A_buffer, CL_FALSE, 0, len * sizeof(float), A, 0, 0, 0
    );
    clEnqueueWriteBuffer(
        command_queue, B_buffer, CL_FALSE, 0, len * sizeof(float), B, 0, 0, 0
    );
    clEnqueueWriteBuffer(
        command_queue, C_buffer, CL_FALSE, 0, len * sizeof(float), C, 0, 0, 0
    );

    // Size specification
    size_t global_work_size = len;

    // Apply the kernel on the range
    status = clEnqueueNDRangeKernel(
        command_queue, kernel, 1, 0, &global_work_size, 0, 0, 0, 0
    );

    printf("EnqueueNDRangeKernel status: %d\n", status);

    // Host buffer <- Device buffer
    clEnqueueReadBuffer(
        command_queue, C_buffer, CL_TRUE, 0, len * sizeof(float), C, 0, 0, 0
    );

    clFinish(command_queue);

    return SUCCESS;
}

int MatrixHadamardProduct(int rows, int cols, float *A, float *B, float *C) {
    if (!lib_global_cl_kernels.vector_kernel_loaded) {
        bool success;
        lib_global_cl_kernels.vector_kernel_program = load_cl_program(
            lib_global_cl_context, "vector_kernel.cl", &success
        );

        if (!success) {
            return FAILURE;
        } else {
            lib_global_cl_kernels.vector_kernel_loaded = true;
        }
    }

    cl_int status;

    // Create a kernel for this call
    cl_kernel kernel = clCreateKernel(
        lib_global_cl_kernels.vector_kernel_program, "hadamard_kernel", &status
    );

    printf("CreateKernel status: %d\n", status);

    // Create the device buffers
    cl_mem A_buffer = clCreateBuffer(
        lib_global_cl_context.context, CL_MEM_READ_WRITE,
        rows * cols * sizeof(float), 0, 0
    );
    cl_mem B_buffer = clCreateBuffer(
        lib_global_cl_context.context, CL_MEM_READ_WRITE,
        rows * cols * sizeof(float), 0, 0
    );
    cl_mem C_buffer = clCreateBuffer(
        lib_global_cl_context.context, CL_MEM_READ_WRITE,
        rows * cols * sizeof(float), 0, 0
    );

    // Set kernel arguments
    clSetKernelArg(kernel, 0, sizeof(cl_mem), (void *)&A_buffer);
    clSetKernelArg(kernel, 1, sizeof(cl_mem), (void *)&B_buffer);
    clSetKernelArg(kernel, 2, sizeof(cl_mem), (void *)&C_buffer);

    // Create the command queue
    cl_command_queue command_queue = clCreateCommandQueueWithProperties(
        lib_global_cl_context.context, lib_global_cl_context.device_id, 0, 0
    );

    // Host buffer -> Device buffer
    clEnqueueWriteBuffer(
        command_queue, A_buffer, CL_FALSE, 0, rows * cols * sizeof(float), A, 0,
        0, 0
    );
    clEnqueueWriteBuffer(
        command_queue, B_buffer, CL_FALSE, 0, rows * cols * sizeof(float), B, 0,
        0, 0
    );
    clEnqueueWriteBuffer(
        command_queue, C_buffer, CL_FALSE, 0, rows * cols * sizeof(float), C, 0,
        0, 0
    );

    // Size specification
    size_t global_work_size = rows * cols;

    // Apply the kernel on the range
    status = clEnqueueNDRangeKernel(
        command_queue, kernel, 1, 0, &global_work_size, 0, 0, 0, 0
    );

    printf("EnqueueNDRangeKernel status: %d\n", status);

    // Host buffer <- Device buffer
    clEnqueueReadBuffer(
        command_queue, C_buffer, CL_TRUE, 0, rows * cols * sizeof(float), C, 0,
        0, 0
    );

    clFinish(command_queue);

    return SUCCESS;
}
