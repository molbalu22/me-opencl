__kernel void
add_kernel(__global float *A, __global float *B, __global float *C) {
    int i = get_global_id(0);
    C[i] = A[i] + B[i];
}

__kernel void
subtract_kernel(__global float *A, __global float *B, __global float *C) {
    int i = get_global_id(0);
    C[i] = A[i] - B[i];
}

__kernel void
hadamard_kernel(__global float *A, __global float *B, __global float *C) {
    int i = get_global_id(0);
    C[i] = A[i] * B[i];
}
