__kernel void multiply_kernel(
    const int rows, const int cols, const int N, __global float *A,
    __global float *B, __global float *C
) {
    int i = get_global_id(0);
    int j = get_global_id(1);

    printf("(%d, %d)", i, j);

    if (i < rows && j < cols) {
        int k;
        float tmp = 0.0f;

        for (k = 0; k < N; k++) {
            tmp += A[i * N + k] * B[k * cols + j];
        }

        C[i * cols + j] = tmp;
    }
}

__kernel void transpose_multiply_kernel(
    const int rows, const int cols, const int N, __global float *A,
    __global float *B, __global float *C
) {
    int i = get_global_id(0);
    int j = get_global_id(1);

    if (i < rows && j < cols) {
        int k;
        float tmp = 0.0f;

        for (k = 0; k < N; k++) {
            tmp += A[k * rows + i] * B[k * cols + j];
        }

        C[i * cols + j] = tmp;
    }
}
